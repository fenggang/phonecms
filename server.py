#!/usr/bin/env python

import os
import sys
import web
import action
import config

class PhoneCMSApplication(web.application):
    def run(self, port=8080, *middleware):
        func = self.wsgifunc(*middleware)
        return web.httpserver.runsimple(func, (config.LISTEN_ADDR, config.LISTEN_PORT))


if __name__ == "__main__":
    theApp = PhoneCMSApplication(action.urls, globals())
    web.config.session_parameters['cookie_name'] = 'webpycms_sid'
    web.config.session_parameters['cookie_domain'] = None
    web.config.session_parameters['timeout'] = 86400,
    web.config.session_parameters['ignore_expiry'] = True
    web.config.session_parameters['ignore_change_ip'] = True
    web.config.session_parameters['secret_key'] = 'JJIEhi323rioes34hafwaj2'
    web.config.session_parameters['expired_message'] = 'Session expired'
    session = web.session.Session(theApp, web.session.DiskStore('data/sessions'), initializer={'login': False})
    def session_hook():
        web.ctx.session = session
    theApp.add_processor(web.loadhook(session_hook))

    theApp.run()
