#!/usr/bin/python

import os
import config

def test_dbmgr():
    import model
    dbmgr = model.dbmgr()

def test_sqlite():
    import sqlite3 as db
    conn = db.connect('/tmp/test.db')
    cursor = conn.cursor()
    sqlList = [
         """
        CREATE TABLE 'news' (
        'id' INTEGER PRIMARY KEY,
        'name' VARCHAR(120),
        'content' TEXT,
        'category' INT,
        'createTime' TIMESTAMP,
        'status' INT
        );

        """
    ]

    for sql in sqlList:
        status = cursor.execute(sql)
    conn.commit()
    conn.close()
    os.unlink('/tmp/test.db')

if __name__ == "__main__":
    test_sqlite()
