jQuery(document).on("pageinit", "[data-role='page']", function(){
    var _j = jQuery.noConflict();
    var page = "#" + _j(this).attr("id");
    var next = _j(this).jqmData("next");
    var prev = _j(this).jqmData("prev");
    if(next){
        _j(page).bind("swipeleft", function(){
            _j.mobile.changePage(next);
        });
    }
    if(prev){
        _j(page).on("swiperight", function(){
            _j.mobile.changePage(prev);
        });
    }
});
