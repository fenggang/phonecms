#!/bin/sh

find . -type f -name "*.pyc" | xargs rm -rf
[ -d data ] && rm -rf data/*
