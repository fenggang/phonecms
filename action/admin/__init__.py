#!/usr/bin/env python

from news import newsAction
from login import loginAction
from logout import logoutAction
from index import indexAction
from upload import uploadAction
from install import installAction
