#!/usr/bin/env python

import web
import os
import config
from log import log
from action.base import action


'''
Admin controller: used for producing views for desktop clients.
'''
class adminAction(action):
    def __init__(self, name=config.NAME, chkLogin=True, chkInstall=True):
        action.__init__(self, name)
        self.tmplDir = os.path.join(config.TMPL_DIR, config.ADMIN_TEMPLATE)
        self.render = web.template.render(self.tmplDir, globals=self.globalsTmplFuncs)
        self.privData['render'] = self.render
        
        if chkLogin and not self.isLogin():
            raise web.seeother('/admin/login/')

        if chkInstall and not self.isInstalled():
            raise web.seeother('/admin/install/')


    def getPageStr(self, url, currentPage, perPageCount, totalCount=10000):
        import math
        totalPage = int(math.ceil(totalCount/perPageCount))
        if '?' in url:
            url=url+'&page='
        else:
            url=url+'?page='
        pageString= ''

        if currentPage > 1:
            pageString += '''
                <span class="alignleft"><a href="'''+url+str(currentPage-1)+'''">&laquo; Prev</a></span>
            '''
        if totalPage>currentPage:
            pageString = pageString+'''
            <span class="alignright"><a href="'''+url+str(currentPage+1)+'''">Next &raquo;</a></span>
        '''
        return pageString

