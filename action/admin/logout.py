#!/usr/bin/env python

import web
import os
import config
from log import log

from base import adminAction

'''
Admin controller: producing admin views.
'''
class logoutAction(adminAction):
    def __init__(self, name = 'Administration'):
        adminAction.__init__(self, name)

    def index(self):
        self.setLogin()
        return self.success('Signed out!', url='/admin/login/')

        
    def GET(self):
        return self.index()        
