#!/usr/bin/env python

import web
import os
import config
from log import log

import action.user
import action.admin

#Mandatory urls
urls = [
    '/admin/',              action.admin.indexAction,
    '/admin/login/(.*)',    action.admin.loginAction,
    '/admin/install/(.*)',  action.admin.installAction,
    '/admin/logout',        action.admin.logoutAction,
    '/admin/upload/(.*)',   action.admin.uploadAction,
]

def fillUrls(name):
    for v in config.MODULES[name]:
        if not v['disabled']:
            urls.append(v['url'])
            urls.append(v['action'])
            

fillUrls('user')
fillUrls('admin')
