#!/usr/bin/env python

import web
import os
import config
from log import log
from base import userAction
from action.modules import modules

'''
User controller: producing music views.
'''
class musicAction(userAction):
    def __init__(self, name = 'Music'):
        userAction.__init__(self, name)
        self.privData['PAGE_ID'] = name

        mods = modules('user')
        prev, next = mods.findNeighborRefsByName(name)
        if not prev or not next:
            prev = next = mods.findRefByName(name)

        self.privData['PAGE_NEXT'] = next
        self.privData['PAGE_PREV'] = prev


    def GET(self, name):
        if not name:
            return self.display('music')
        else:
            return self.notFound()
