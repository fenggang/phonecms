#!/usr/bin/env python

import web
import os
import config
from log import log
from base import userAction
from action.modules import modules

'''
User controller: producing index views.
'''
class indexAction(userAction):
    def __init__(self, name = 'Home'):
        userAction.__init__(self, name)
        self.privData['PAGE_ID'] = name

        mods = modules('user')
        prev, next = mods.findNeighborRefsByName(name)
        if not prev or not next:
            prev = next = mods.findRefByName(name)

        self.privData['PAGE_NEXT'] = next
        self.privData['PAGE_PREV'] = prev
        

    def GET(self):
        return self.display('index')
