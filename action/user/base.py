#!/usr/bin/env python

import web
import os
import config
import utils
from log import log

from action.base import action
from action.modules import modules

'''
User controller: userd for producing views for embedded clients.
'''
class userAction(action):
    def __init__(self, name=config.NAME):
        action.__init__(self, name)

        self.tmplDir = os.path.join(config.TMPL_DIR, config.TEMPLATE) 
        self.render = web.template.render(self.tmplDir, globals=self.globalsTmplFuncs)
        self.privData['render'] = self.render
        mods = modules('user')
        self.privData['PAGE_FOOTER_MODS'] = mods.get()

        if not self.isInstalled():
            raise web.seeother('/admin/install/')
