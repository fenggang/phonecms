#!/usr/bin/env python

import web
import os
import config
from log import log
from base import userAction
from model.news import news as modelNews
from action.modules import modules

'''
User controller: producing news views.
'''
class newsAction(userAction):
    def __init__(self, name = 'News'):
        userAction.__init__(self, name)
        self.privData['PAGE_ID'] = name

        mods = modules('user')
        prev, next = mods.findNeighborRefsByName(name)
        if not prev or not next:
            prev = next = mods.findRefByName(name)

        self.privData['PAGE_NEXT'] = next
        self.privData['PAGE_PREV'] = prev


    def index(self):
        condition = {}
        self.privData['newsList'] = modelNews().getList('*', condition, 'id desc')
        return self.display('news')

    def GET(self, name):
        # News: index page
        if not name:
            return self.index()
        if name == 'index':
            return self.index()
        elif name == 'show':
            return self.show()
        # News: page not found
        else:
            return self.notFound()

    def show(self):
        inputParams = self.getInput()
        if not inputParams.has_key('id') :
            web.seeother(config.WEB_URL)
        id=inputParams['id']
        condition = {'status':1,'id':str(id)}
        item = modelNews().getOne('*',condition)
        if item == None:
            raise web.notfound('not found')
        self.privData['NEWS_ITEM'] = item
        return self.display('newsshow')
