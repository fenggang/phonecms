#!/usr/bin/env python
#coding=utf-8
import os

#Debugging
DEBUG=1

#Site information
LISTEN_ADDR='127.0.0.1'
LISTEN_PORT=8080
URL='http://%s:%d' % (LISTEN_ADDR, LISTEN_PORT)
NAME='PhoneCMS'
DESCRIPTION='PhoneCMS is designed for embedded devices'

#Theme
TEMPLATE='default'
ADMIN_TEMPLATE='admin'
COUNT_PER_PAGE=10

#Admin
ADMIN_USERNAME='admin'
ADMIN_PASSWORD='admin'

#Path
ROOT_DIR=os.getcwd()+'/'
DATA_DIR=ROOT_DIR+'data/'
TMPL_DIR=ROOT_DIR+'template/'
STATIC_DIR=ROOT_DIR+'static/'
UPLOAD_DIR=DATA_DIR+'uploads/'

#Max alloawd upload file size (2M by defualt)
MAX_UPLOAD_FILE_SIZE=2*1024*1024

#Database
DB_TYPE='sqlite'
DB_NAME='%s.db' % NAME

#Language
LANGUAGE='default'

#Modules
MODULES = {}
MODULES['user'] = [
    # Home modle
    {
        # Default name of this module
        'name.default'  : 'Home',
        # Used for determing the postion within the footer menu
        'id'            : 0,
        # Relative reference to this module
        'ref'           : '/',
        # Webpy url/action mapping
        'url'           : '/',
        'action'        : 'action.user.indexAction',
        # Whether disable this module or not
        'disabled'      : 0,
    },

    # News module
    {
        'name.default'  : 'News',
        'id'            : 1,
        'ref'           : '/news/',
        'url'           : '/news/(.*)',
        'action'        : 'action.user.newsAction',
        'disabled'      : 0,
        'thumbnail'     : {'xres': 64, 'yres': 64, 'quality': 80},
    },

    # Gallery module
    {
        'name.default'  : 'Gallery',
        'id'            : 2,
        'ref'           : '/gallery/',
        'url'           : '/gallery/(.*)',
        'action'        : 'action.user.galleryAction',
        'disabled'      : 0,
        'cover.thumbnail' : {'xres': 160, 'yres': 90, 'quality': 80},
        'image.thumbnail' : {'xres': 480, 'yres': 320, 'quality': 80},
    },

    # Music module
    {
        'name.default'  : 'Music',
        'id'            : 3,
        'ref'           : '/music/',
        'url'           : '/music/(.*)',
        'action'        : 'action.user.musicAction',
        'disabled'      : 0,
    },

    # Settings module
    {
        'name.default'  : 'Settings',
        'id'            : 4,
        'ref'           : '/settings/',
        'url'           : '/settings/(.*)',
        'action'        : 'action.user.settingsAction',
        'disabled'      : 0,
    },
]

MODULES['admin'] = [
    # Home modle
    {
        'name.default'  : 'News',
        'id'            : 1,
        'ref'           : '/admin/news/list',
        'url'           : '/admin/news/(.*)',
        'action'        : 'action.admin.newsAction',
        'disabled'      : 0,
    },
]

